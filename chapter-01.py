import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from tsprnn.UnivariateLSTMPredictor import UnivariateLSTMPredictor

if __name__ == '__main__':
    # load and preprocess
    epd = pd.read_csv('./data/gmails-per-day.csv')
    epd['date'] = epd.date.astype('str')
    epd['date'] = pd.to_datetime(epd['date'], format='%y%m%d')
    epd = epd.set_index(['date'])

    plt.figure(1, figsize=(11, 8.5))
    plt.subplot(2, 1, 1)
    epd.number[0:21].plot()
    plt.title('Emails for Weeks 1, 2, 3')
    plt.subplot(2, 1, 2)
    epd.number[21:42].plot()
    plt.title('Emails for Weeks 4, 5, 6')
    plt.show()

    epd_data = epd.number.values
    training_data = epd_data[:200] # first 200 points for training
    testing_data = epd_data[200:] # rest for testing

    uniLSTMPredictor = UnivariateLSTMPredictor(
        window_size=3,
        look_ahead=2,
        hidden_layer_dimensions=[50, 50],
        num_epochs=200)

    uniLSTMPredictor.train(training_data)
    uniLSTMPredictor.visualize_training([1,2])

    uniLSTMPredictor.predict(testing_data)
    uniLSTMPredictor.visualize_testing([1,2])