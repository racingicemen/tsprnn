---
title: "Timeseries Prediction"
subtitle: "using Recurrent Neural Networks in Keras"
author: [Rajesh Kommu]
date: \today
tags: [RNNs, Keras, LSTMs, Timeseries]
titlepage: true
titlepage-rule-height: 6
toc: true
classoption: twoside
titlepage-text-color: "000000"
titlepage-rule-color: "cb5414"
# titlepage-color: cb5414
link-citations: true
---

# Timeseries
## Introduction
Timeseries are everywhere. Any dynamical process or system that generates one or more outputs is a 
source of timeseries data. 

> A timeseries is a **time-ordered sequence** of one or more data points. A univariate timeseries has a
single sequence, while a multivariate timeseries has multiple sequences, with possible interactions between
the variables generating the multiple sequences.

The key point here is the time-ordered nature of the values in the sequence. Every data point in a timeseries
has an associated timestamp. This timestamp may be explicitly recorded witht the data point, or it might be
implied. In either case, a sequence of values 1, 2, 3, ... is different from a sequence 3, 1, 2, ... since
the values appear in different time order.

There are many examples of timeseries data, such as 

* Stock market data for various stocks. Daily values of multiple parameters, such as opening price, 
closing price, volume, etc. are recorded for a multitude of stocks, ranging over many years. This is a
canonical example of stock market data. 
* Temperature, relative humidity, and other weather parameters recorded for a specific location over a time
interval.
* Monthly sunspot numbers recorded over many years

More examples of timeseries datasets can be found [here](https://archive.ics.uci.edu/ml/datasets.html?format=&task=&att=&area=&numAtt=&numIns=&type=ts&sort=nameUp&view=table)

## Nature of Timeseries Analysis 
Timeseries analysis is usually performed for one of two reasons:

1. Regression: Here we are trying to **predict** the future values of the timeseries data. E.g., in case of
the stock market data, being able to predict this accurately would be obviously financially beneficial.
In case of weather data, being able to predict the future timeseries values could help with planning for
any weather related issues, etc. Timeseries regression is a supervised learning problem.

2. Classification: The goal here is to use to timeseries data points seen up to the present to determine
the class to which the future state of the system will belong. This is somewhat similar to the regression 
case listed above, but instead of predicting specific values of the system, we are interested in classifying
the state of the system.

## Mini Case Study
In this section, we go through a simple case study, using a rather silly example of a timeseries. While
the timeseries might be somewhat silly and small (only about 300 data points), this case study illustrates
almost all of the steps involved in settig up and making predictions using RNNs. (RNNs are explained and
described in a later chapter).

### The Timeseries: Emails Per Day
The dataset consists of emails received per day in my personal gmail account. Starting from April 14, 2018
I recorded the number of emails received per day, in my personal gmail account, in a ```.csv``` file. The
format of the data is ```YYMMDD, N``` where ```YYMMDD``` is the date of the recording, and ```N``` is number
of emails for that date. A point to make note of is that there are no ```"``` around ```YYMMDD```.

### Obtaining the dataset
Since I was recording the data, obtaining the dataset was straigtforward; just locate the file on the 
filesystem.

### Preprocessing the dataset
First we load the dataset into a ```pandas DataFrame``` object and get some information about the dataset.
```python
import pandas as pd
epd = pd.read_csv('gmails-per-day.csv')
epd.info()
```
This displays the following:
```sh
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 295 entries, 0 to 294
Data columns (total 2 columns):
date      295 non-null int64
number    295 non-null int64
dtypes: int64(2)
memory usage: 4.7 KB
```
This tells us that the first column is being treated as a 64-bit integer. We want this column to be a date.
This conversion happens in two steps:

1. Conver the column from an integer to a string.
1. Parse the string as YYMMDD formatted date.

This is shown in the following code snippet:
```python
epd['date'] = epd.date.astype('str')
epd['date'] = pd.to_datetime(epd['date'], format='%y%m%d')
epd.info()
```
Now we see that the first column is a datetime, as desired.
```sh
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 295 entries, 0 to 294
Data columns (total 2 columns):
date      295 non-null datetime64[ns]
number    295 non-null int64
dtypes: datetime64[ns](1), int64(1)
memory usage: 4.7 KB
```
But how does the ```DataFrame``` contents look like?
```python
epd.head()
```
displays
```sh
        date  number
0 2018-04-14      19
1 2018-04-15      19
2 2018-04-16      36
3 2018-04-17      43
4 2018-04-18      42
```
Since we did not specify the column that is to be used as the index, pandas adds its own column. We need
to fix this
```python
epd = epd.set_index(['date'])
```
Now the ```DataFrame``` looks like:
```sh
            number
date              
2018-04-14      19
2018-04-15      19
2018-04-16      36
2018-04-17      43
2018-04-18      42
```
At this point, we are ready to do some premliminary visual analysis.

### Preliminary Visual Analysis of the Timeseries

First, let's visually inspect the entire time series

```python
import matplotlib.pyplot as plt
epd.plot()
plt.show()
```

![Emails Per Day\label{epd-alldata}](figures/epd-alldata.png){width=60%}

This looks a bit noisy, so let's look at the first six weeks data, split into three week chunks.

![Emails Per Day, First Six Weeks \label{epd-firstsix}](figures/epd-firstsixweeks.png){width=60%}

We notice weekly cycles in both plots, with troughs of the cycles in weekends. This sort of makes sense
since the emails I receive are mostly from entities that are more active during the week than in the 
weekend.

### Setting Up the RNN in Keras

Next we look at timeseries prediction using RNNs in Keras. The details will be presented in a later chapter;
for now, we just use a Python class that wraps the details for us.

The first step is to extract the actual data, sans the timestamps, from the ```epd DataFrame``` object and
split it into training and testing data sets. (For the actual case studies presented in a later chapter, 
we will also have a validation data set.)

```python
epd_data = epd.number.values

training_data = epd_data[:200] # first 200 points for training
testing_data = epd_data[200:]  # rest for testing
```
Next we create an instance of the ```UnivariateLSTMPredictor``` class:
```python
uniLSTMPredictor = UnivariateLSTMPredictor(
        window_size=3,
        look_ahead=2,
        hidden_layer_dimensions=[50, 50],
        num_epochs=200)
```
This class predicts the future values of a univariate timeseries using an Long Short-Term Memonry RNN. We
use ```window_size``` number of past data points to predict the value of ```look_ahead``` number of future
data points. The ```hidden_layer_dimensions``` parameter tells us the number and dimensions of the hidden
layers in the neural network topology. ```num_epochs``` tells us the number of epochs for which training
is to be done.

Once the predictor class has been instantiated, the next step is to train the network
```python
uniLSTMPredictor.train(training_data)
```
The results of the training are stored internally in the class. 
Next, we evaluate the performance of the trained network by predicting future values.
```python
uniLSTMPredictor.predict(testing_data)
```
The results of testing are also store internally. 
Finally, we visualize the results.
```python
uniLSTMPredictor.visualize([1, 2])
```
The list of values passed in to ```visualize``` contain the k-steps ahead that we are interested in 
evaluating the performance on. In this example, we want to visualize 1-step ahead and 2-step ahead.

Running the above code with the parameter values shown produces the following:

![1- and 2- step ahead prediction, window size=3, 2 hidden layers \label{ch01-run01}](figures/ch01-run01.png){width=60%}

We observe the following from the plot:

* Performance on the training data is better than that on the testing data
* Predictor seems to have identified the cyclical nature of the data and is able to track the weekly cycles
* On the test data set, 1-step ahead prediction seems to be better than the 2-step ahead prediction. 

### Exploring the Parameter Space
There are multiple ways to explore the parameter space, a few of which are 

1. Changing the number of hidden layers
1. Changing the dimensions of the hidden layers
1. The number of epochs that the training is done for
1. The ```window_size``` vs the ```look_ahead``` ratio.

In this section, we explore two of these, namely increasing the number of hidden layers to three, and asking the 
predictor to predict further out ahead (```look_ahead=6```)

First, the 2-step ahead prediction with 3 hidden layers, of 50 nodes each. The results are shown in Figure
\ref{ch01-run02}. 
```python
uniLSTMPredictor = UnivariateLSTMPredictor(
        window_size=3,
        look_ahead=2,
        hidden_layer_dimensions=[50, 50, 50],
        num_epochs=200)
```
![1- and 2- step ahead prediction, window size=3, 3 hidden layers \label{ch01-run02}](figures/ch01-run02.png){width=60%}

We notice no significant improvement in performance on test data suggesting that two hidden
layers might be sufficient for this data set.

Next, we evaluate the predictor's performance in predicting further out ahead, using the same window size
of past data values. The 6-step ahead prediction with 3 hidden layers, of 50 nodes each is shown in Figure 
\ref{ch01-run03}

![1-, 2-, 6- step ahead prediction, window size=3, 3 hidden layers \label{ch01-run03}](figures/ch01-run03.png){width=60%}

We notice that the predictor's performance is worse the further out into the future it tries to predict.

### Summarizing the Case Study
The simple, small emails-per-day dataset proved to be useful for a quick walk through of the time series
analysis process that we will discuss in detail in subsequent chapters. In brief, this process is:

1. Obtain the dataset. This most likely involves accessing an external data source.
1. Pre-process the data set. This involves identifying how to represent the timeseries data in code. For
```python```, ```pandas.DataFrame``` and ```numpy.array``` are the most obvious choices. Pre-processing also
involves identifying missing data, converting datestamps and timestamps into the appropriate format, identifying
the attributes that will be used in the analysis, etc.
1. Preliminary visual analysis. This let's us get a feel for the data, and identify and trends or patterns
that we expect the prediction process to handle.
1. Setting up the predictor. We use ```UnivariateLSTMPredictor``` that uses Keras to implement its functionality.
1. Playing around with different parameter settings, to identify the parameter(s) for making the best possible
predictions.

## Outline of the Report
Chapter 2 discusses Recurrent Neural Networks in detail. This discussion also includes the Long Short-Term
Memory Network architecture. Then we talk about how LSTMs are implemented in Keras, followed by the 
a discussion of the ```UnivariateLSTMPredictor``` class. Chapter 3 presents a case study using time series 
data from the financial sector. Finally, Chapter 4 wraps up the report.

# Recurrent Neural Networks
> Figures \ref{twentytwoDOTten}, \ref{twentytwoDOTfourteen}, \ref{twentytwoDOTtwentyone}, 
\ref{unrolled}, \ref{twentytwoDOTtwentysix}, \ref{twentytwoDOTtwentyseven}, \ref{twentytwoDOTtwentyeight},
\ref{twentytwoDOTtwentynine} are reproduced from [Deep Learning: From Basics to Practice, Volume 2](https://github.com/blueberrymusic/DeepLearningBookFigures-Volume2). The original author of these figures is
Andrew Glassner, who has published these figures under the MIT License. This is greatly appreciated.

## Introduction
RNNs are used for processing sequences. Before we get into the details of RNNs, let's first define what 
sequences are.

A sequence is a collection of data samples where the order of the samples in the collection matters. 
Timeseries obviously are collections. So are sentences in a natural language. Jumbling up the words in a 
sentence leaves the collection unchanged, but the sentence would be gibberish. Sequences are better 
understood when they are studied in their entirety.

Once a sequence has been processed and the structure and meaning of the sequence has been extracted, we can
use the processing algorithm (a trained RNN in our case) to generate new sequees or to predict the future
values in the sequence. Examples include stock market prediction or generating new text by training an RNN
on an existing corpus.

## Sequence Prediction as a Supervised Learning Problem
It is possible to use feedforward neural networks and convert the problem of processing sequences into a 
standard feedforward problem. To see how this is done, consider the following sequence of scalar (i.e. 
1-dimensional) values:
```
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+----
|  3  |  8  |  7  |  5  |  8  |  9  |  6  |  4  |  8  |  9  | 11  |  8  |  7  |  8  | ...
+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+-----+----
```

Let's say we use the first ```M``` samples to predict the next ```P``` samples. E.g., with ```M=5,P=2```
we would get:

```
+-----+-----+-----+-----+-----+         +-----+-----+
|  3  |  8  |  7  |  5  |  8  |   --->  |  9  |  6  |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```

Now imagine sliding over one position to the right. The next sample would be:
```
+-----+-----+-----+-----+-----+         +-----+-----+
|  8  |  7  |  5  |  8  |  9  |   --->  |  6  |  4  |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```
We keep moving one position to the right, generating pairs of vectors of length 5 and 2, respectively, till
we reach the end of the sequence. Then we can use these pairs to train a feedforward neural network in a
supervised fashion.

Once the feedforward NN is trained, we could present a sequence of previously unseen values (i.e. unseen
during training), and expect it to predict the next two values in the sequence:
```
+-----+-----+-----+-----+-----+         +-----+-----+
| x1  | x2  | x3  | x4  | x5  |   --->  |  y1 |  y2 |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```
This should work reasonably well, and it does so in simple cases. So why bother inventing a whole new class
of (complicated) neural networks? The answer lies in the fact that the above technique has a serious limitation
in processing input **sequences**. To understand this, consider the first input-output pair. As far as the 
feedforward NN is concerend
```
+-----+-----+-----+-----+-----+         +-----+-----+
|  3  |  8  |  7  |  5  |  8  |   --->  |  9  |  6  |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```
is the same as
\pagebreak

```
+-----+-----+-----+-----+-----+         +-----+-----+
|  8  |  5  |  8  |  3  |  7  |   --->  |  9  |  6  |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```
which is the same as
```
+-----+-----+-----+-----+-----+         +-----+-----+
|  5  |  3  |  7  |  8  |  8  |   --->  |  6  |  9  |
+-----+-----+-----+-----+-----+         +-----+-----+ 
```
and so on. The sequential structure within an input and output sample is being completely ignored! This flies
in the face of our starting point that there is some intrinsic meaning/structure to the **order** of input
and output values.

To actually make use of this sequential information, we need neurons (units) that are capable of remembering
internal information over time. This internal information is called **state**.

## State
State is defined as internal information that a neuron remembers; i.e. it is the neuron's memory. Each time
the unit processes input, the state from previous input processing is available for use. A key point is
that *the state changes with **every** input*, even after training is complete. The state can be updated 
even when a trained RNN is making predictions.

The memory we are talking about is different from the usual weights associated with a NN. Weights are
updated only during training, and remain fixed after the training is complete.

In an RNN, state is usually a collection of floating point numbers. The size of the collection determines
the size of the state/memory. Obviously, we want the size of the state to be just right -- not too big, not
too small.

We can depict the RNN processing an input using the following schematic:

![Unrolled RNN\label{twentytwoDOTten}](figures/Figure-22-010.png){width=60%}

The leftmost state block represents the initial state, before any input has been processed. The second 
state block is the updated state after ```input0``` has been processed, but before ```input1``` has been
processed. The key point is that both blocks refere to the **same** state vector, just with updated values.
Figure \ref{twentytwoDOTten} is showing a **single RNN unit**, either being trained with a sequence of
inputs and outputs, or processing a sequence of inputs and generating a sequence of outputs. There is a 
single state block, that gets updated during both training and testing. The inputs and outputs can be either
single-valued (**scalars**) or mulit-dimensional (**tensors**). Figure \ref{twentytwoDOTten} depicts what
is commonly referred to as the *unrolling* of an RNN. When an input arrives at time ```(t)```, it is combined
with the state at time ```(t-1)``` to produce output at time ```(t)``` and the update state at time ```(t)```.
This updated state is available to process the input at time ```(t+1)``` and so on.

## Internals of an RNN cell
How is the input combined with the state? The simplest way to combine numerical values is to multiply and 
add them. We scale the input and state (multiply) and then combine the scaled values (add). We also scale
the generated outputs by multiplying them with weights, as shown in Figure \ref{twentytwoDOTfourteen}

![Combining Input and State\label{twentytwoDOTfourteen}](figures/Figure-22-014.png){height=24%}

The small solid block on the right is a **time-delay** operation, indicating that scaled state at previous
time step is combined with scaled input at current time step to produce scaled output at current timestep. 
Figure \ref{twentytwoDOTfourteen} also gives us a first look at what it means to train an RNN --- it involves
updating the three sets of weights!

As mentioned before, input, output and state will be vectors in the general case, and the weights will be 
matrices of compatible dimensions.

## Inputs presented to an RNN
The sequential data presented to an RNN for training consists of **samples**. Each sample has a certain
number of **features**, and each feature is made up of certain number of **time steps**. Let us look at an
example to understand these terms better:

Let's say we want to make daily weather recordings at a particular location. We decide to record three
observations for three quantities --- temperature, relative humidity and wind speed. We collect data for a
week, and each day, the sensors record the data from 9 am to 4 pm, at hourly intervals. Then:

* Since we collected data for a week, number of **samples equals 7**.
* Since we are recording data for three different quantities, number of **features equals 3**.
* Since each daily measurement consists of eight houely sensor recordings, number of **time steps equals 8**.

For most sequences, it is common to have timesteps equal to 1. Figure \ref{twentytwoDOTtwentyone} illustrates
how this three dimensional data is organized.

![Three dimensions of input data\label{twentytwoDOTtwentyone}](figures/Figure-22-021.png){height=24%}

A key point to remember is that **every feature needs to have the same number of time steps and samples**.

## Training a RNN

Consider the following unrolled form of the RNN, as first shown in figure \ref{twentytwoDOTten}.

![Unrolled RNN\label{unrolled}](figures/Figure-22-010.png){width=60%}

To train an RNN we use a technique called Back Progagation Through Time (BPTT). BPTT is similar to standard
Back Propagation in that it pushes error gradients backwards through the unrolled RNN; however, BPTT is aware
of the fact that each instance of the RNN represents the same unit with the same set of weights.

While BPTT works, it suffers from two serious problems

1. The **vanishing gradient problem** arises from applying the ```tanh``` activation function repeatedly.
This causes the output values to move closer to zero, and the region of non-zero gradients gets smaller.

2. The **long-term dependency problem** is the RNN trying to remember everything! For any finite amount of
memory, the RNN will eventually encounter a sequence too long to be remembered in its entirety. It's like a 
hundred pages into reading a book, we are still trying to remember the first sentence of the book!

Both these problems are addressed by a more advanced form of RNN called **L**ong **S**hort-**T**erm **M**emory
Network (LSTM).

## LSTMs
Long Term memory is the RNN unit's state that we discussed above. The influence of the values in the state
can survive indefinitely, from one input to the next. Short Term memory of the cell refers to the output
values of the cell. These values are recomputed every time a new input is processed to compute the cell's
output.

The basic idea behind the LSTM is to persist the short-term memory for a longer time. So the state computed
while processing ```input1``` will hang around while processing ```input2```, a property that clearly should
help in the processing of sequential inputs! The LSTM also has the ability to forget memory it no longer
needs. This addresses the long-term dependency problem.

This remembering and forgetting is done by internal networks inside each LSTM unit/cell. In a correctly trained
LSTM, the weights in the internal networks will be such that an optimal amount of memory will be remembered
and forgotten! While the weights of these internal networks don't change after training is complete, what
is remembered and forgotten depends on the current input being processed.

### Gates
Gates are central to the implementation of LSTMs. A **Gate** is a device that enables the remembering and
forgetting of a value. To completely remember a value, a gate multiplies the value by 1.0 To completely forget
a value, the gate multiplies the value by 0.0 

#### Forgetting values in memory
The following figure illustrates the process of forgetting a set of existing values:

![Forgetting a set of existing values.\label{twentytwoDOTtwentysix}](figures/Figure-22-026.png){width=25%}

Figure \ref{twentytwoDOTtwentysix}(a) shows how a vector of values is multiplied by a dimensionally compatible
vector of gate values to produce modified values. There is 80% remembrance of the first value, the second
value is completely remembered, the fourth value is completeley forgotten, etc. Figure \ref{twentytwoDOTtwentysix}(b)
shows the same process in a schematic fashion, clearly showing that the old value of the memory is overwritten
by the modified values. 

#### Assimilating a new value into memory
Assimilating a new value is defined as how much influence the new value has on existing values. This takes
place in two steps:

1. Multiply new values by gate values
2. Add values from step 1. to existing values.

If the gate multiplier is 0, we choose to not assimilate the new value at all -- the new value has no influence
on the existing memory. The following diagram shows the process of assimilating a set of new values

![Assimilating a set of new values.\label{twentytwoDOTtwentyseven}](figures/Figure-22-027.png){width=35%}

Figure \ref{twentytwoDOTtwentyseven}(b) shows the process of assimilation in a schematic fashion, showing 
that the old value of the memory both participates in the assimilation process and is overwritten
by the assimilation process.

#### Selecting from memory
Selecting from memory involves the single step of multiplying the memory by a gate value. This determines
how much of each memory location we want to use

![Selecting values from memory.\label{twentytwoDOTtwentyeight}](figures/Figure-22-028.png){width=35%}

### LSTMs
LSTMs use gates to manage the remembering and forgetting of sequential information. The following diagram
shows the structure of an LSTM cell.

![Internals of an LSTM cell.\label{twentytwoDOTtwentynine}](figures/Figure-22-029.png){width=50%}

The inputs to the LSTM cell are new inputs at the current time step, and output from the previous time step.
The output of the LSTM cell is the output at the current time step. There are three gates in the cell:

1. The **forget gate ```F```**
2. The **assimilation gate ```R```**
3. The **select gate ```S```**

There are four collections of nuerons, identified by A, B, C, and D. The input to these four collections is
the current input combined with the previous output. This combination is done by simply appending the input
list to the output list.

There are three steps involved in the processing of an input:

1. **The forgetting step**: In the forgetting step, new input and previous output are used by neural network
```A``` to create gate values. Since gate values are required to belong to the ```[0,1]``` interval, the neurons
in ```A``` need to have the sigmoid activation functions. The output of ```A``` is fed to the forget gate
```F```, which applies it to the current state.

*What the LSTM cell is trying to do in this step is to forget some of it's past state based on the new input
and the output generated in the previous step.*

2. **The assimilation step**: In this step, the previous output + current input combination is sent to neural
networks ```B``` and ```C```. ```C``` produces outputs that will be used as gate values, so the activation function needs to
be sigmoid. ```B``` produces outputs that is being gated, and is thus being assimilated. The assimilated ```B``` values
are combined with the forgotten state values to create the new state values.

*What the LSTM cell is trying to do in this step is assimilate new input into existing state; it is trying
to learn something new*

3. **The selection step**: In this step, previous output + current input is sent to neural network ```D``` to
generate gate values for the selection gate ```S```. Selection gate ```S``` selects from the new state which
has been passed through the ```tanh``` activation function. ```tanh``` ensures the new state values lie in
the ```[-1, +1]``` interval.

*In this step, the LSTM cell is trying to select important features from the new state*

> Summarizing the three steps
>
> 1. Forget some part of current state.
> 2. Assimilate some part of new input.
> 3. Select something from the combination of 1 and 2.

* The activation functions within an RNN cell are not applied repeatedly. The activation function that the
cell output is sent through is the linear function. These two features together combine to avoid the 
vanishing gradient problem.

* The forget and assimilate gates control what needs to be assimilated and forgotten. This takes care of the
long-term dependency problem.

## LSTMs in Keras
Keras provides two APIs for implementing LSTMs

* Sequential
* Functional

In this report, we exclusively use the Sequential API and that is what we will discuss in detail below. In
the sequential API, the model is built in a sequential fashion, starting from the input layer on
the left and working our way to the output layer on the right. The following code shows how this
is done:

```python
from keras.models import Sequential
from keras.layers import LSTM, Dense

model = Sequential()
model.add(LSTM(50, activation='relu', input_shape=(3, 1))
model.add(Dense(1))
model.compile(optimizer='adam', loss='mse')
```
In the above code block, we are doing the following:

1. Create an instance of the ```keras.models.Sequential``` class.
1. Add an instance of ```keras.layers.LSTM``` as the single hidden layer to the ```Sequential``` model.
1. Add an instance of ```keras.layers.Dense``` as the output layer to the ```Sequential``` model.
1. Compile the model.

Let's look at steps 2-4 in detail.

* The first argument to the ```LSTM``` class is the number of nodes in the hidden layer, 50 in this case.
* The second argument is the activation function. The ```relu``` activation is the REcitified Linear Unit
whose output is shown in Figure \ref{relu}. This is the most commonly used activation function RNNs, since 
it minimizes the vanishing gradient problem discussed earlier.

![Output of Rectified Linear Unit.\label{relu}](figures/relu.png){width=25%}

* We didn't need to specify an input layer. The reason for this is the Keras' Sequential model takes sets up
the input layer for us, provided we correctly specify the ```input_shape``` parameter on the **first** 
hidden layer.
* ```input_shape``` is a tuple. Referring back to the supervised learning scenario discussed before, the first value of ```input_shape``` is the dimension of the input in the supervised learning problem. In other
words, it is the number of past data points we will be using to predict future values.
* The second value of ```input_shape``` is the number of features we are trying to predict. In the case of
univariate problems, this value is 1
* ```LSTM``` has another parameter, not shown above, called ```return_sequences```. This should be set to
true for all layers **except the last one**. In this case, we have only one layer, so we set it to false, 
which is the default value.
* The argument to the ```Dense``` object specifies the number of outputs being predicted. In other words,
this is the value of ```k``` in the k-steps ahead prediction we are trying to make.
* Finally, we compile the Sequential model. This prepares the model for training. The ```optimizer='adam'```
tell us that we are using the Adam optimization algorithm. Adam, whose name is derived from Adaptive 
Momentum, is an alternative to the Stochastic Gradient Descent method for updating the weights of the neural
network. Adam has many adavantages and combines the benefits of Adaptive Gradient (AdaGrad) and Root Mean 
Square Propagation (RMSProp) algorithms.
* The second argument to the ```compile``` function specifies the definition of loss used in the network.
Loss quantifies the error between the predicted and actual output. ```mse``` stands for Mean Squared Error
and is the most logical choice for time series prediction problems. Time series prediction involves numerical
quantities, and we are doing regression. Whether the predicted value is more by 1.0 or less by 1.0, compared
to the actual output, we have the same amount of error. Hence the squared error.

## Implementation of ```UnivariateLSTMPredictor```
Instead of directly making calls into the Keras API, we implement a simple class, 
```UnivariateLSTMPredictor``` that wraps the calls for us. First the high-level definition of this class

### High-level class overview
```python
import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM
import pandas as pd

class UnivariateLSTMPredictor:
    def __init__(self, window_size, look_ahead, hidden_layer_dimensions, num_epochs):
        ...    
    def initialize_sequential_model(self):
        ...
    def split_sequence(self, raw_sequence):
        ...
    def train(self, training_data):
        ...
    def predict(self, testing_data):
        ...
    def visualize_training(self, lookaheads):
        ...
    def visualize_testing(self, lookaheads):
        ...
    def visualize_validation(self, lookaheads):
        ...
    def plot_loss_vs_epochs(self):
        ...
```

### Constructor

The constructor to this class takes the following arguments:

1. ```window_size``` specifies the number of past data points used for predicting future values. E.g.
        ```window_size=4```.
1. ```look_ahead``` specifies the number of steps out we are trying to predict. This is the ```k``` in the
        k-steps ahead prediction. E.g. ```look_ahead=2```.
1. ```hidden_layer_dimensions``` is list of integer values, specifying the number and dimensions of the
        hidden layers in the network. E.g. ```hidden_layer_dimensions``` ```=[50,50]``` specifies that 
        there are two hidden layers, each with 50 nodes.
1. ```num_epochs``` is the number of epochs over which training is carried out. An epoch is defined as
        one pass through all the values in the input data set.
After recording all the values, the constructor calls the ```initialize_sequential_model``` method, which 
is an internal/hidden method of the ```UnivariateLSTMPredictor``` class.

### ```initialize_sequential_model```

```initialize_sequential_model``` is defined as follows:
```python
    def initialize_sequential_model(self):
        self.model = Sequential()
        self.model.add(
            LSTM(self.hidden_layer_dimensions[0],
                 activation='relu',
                 input_shape=(self.window_size, self.num_features),
                 return_sequences=True if self.num_hidden_layers > 1 else False))
        for ndx in range(1, self.num_hidden_layers):
            self.model.add(
                LSTM(self.hidden_layer_dimensions[ndx],
                     activation='relu',
                     return_sequences=True if ndx < self.num_hidden_layers-1 else False))
        self.model.add(Dense(self.look_ahead))
        self.model.compile(optimizer='adam', loss='mse')
```
The details of this method should be fairly obvious. It first creates a Sequential model, a reference to
which is stored as an instance variable, ```self.model```. It then adds the specified number of ```LSTM```
hidden layers, with the specified number of nodes in each hidden layer. This method ensures that the first,
and only the first, hidden layer specifies the ```input_shape``` parameter. The values in this tuple are
the ```self.window_size``` parameter, which is specified in the constructor, and the ```self.num_features```
instance variable, whose value is set to 1 in the constructor, since this is a univariate predictor. 
```self.num_features``` serves a documentation purpose, since it avoids the use of a "magic" value of 1.
```initialize_sequential_model``` also ensures that all but the last hidden layer have 
```return_sequences=True``` 

After the hidden layers have been constructed, ```initialize_sequential_model``` constructs a ```Dense```
output layer. The number of nodes in the output layer are specified to be the number of steps ahead the 
predictor is predicting, a value that the user specifies in the constructor and which is stored in the
```self.look_ahead``` instance variable.

Finally, ```initialize_sequential_model``` compiles the sequential model, getting it ready for the next step.

### ```split_sequence```
```split_sequence``` is defined as follows:
```python
    def split_sequence(self, raw_sequence):
        inputs = []
        outputs = []
        for i in range(len(raw_sequence)):
            input_start_index = i
            input_stop_index = input_start_index + self.window_size
            output_start_index = input_stop_index
            output_stop_index = output_start_index + self.look_ahead
            if output_stop_index <= len(raw_sequence):
                inputs.append(raw_sequence[input_start_index:input_stop_index])
                outputs.append(raw_sequence[output_start_index:output_stop_index])
        return np.array(inputs), np.array(outputs)
```
This method sets up the supervised learning problem previously discussed. It splits a "raw" sequence into
an input array and an output array. The input array size depends on the ```self.window_size``` parameter 
and the output array size depends on the ```self.look_ahead``` parameter. ```split_sequence``` is also an 
internal method, that is called by the ```train``` and ```predict``` methods.

### ```train```
```python
    def train(self, training_data):
        self.train_input, self.train_output = self.split_sequence(training_data)
        input_shape = self.train_input.shape
        self.train_input = self.train_input.reshape((input_shape[0], input_shape[1], self.num_features))
        self.history = self.model.fit(self.train_input, self.train_output, epochs=self.num_epochs, verbose=0)
        self.train_predict = self.model.predict(self.train_input)
```
The ```train``` method is called by the user specifying the sequence that is to be used for training. First
the training sequence is split into the input, output pairs. Next, the input is *reshaped*, since Keras 
expects its input values in a certain shape. The actual training happens in the call to the ```fit``` method
of the model. The training history is recorded in the ```self.history``` instance variable. This is used
later to visualize how the training progresses. Once training has been completed, the performance of the
model on the *training data* is evaluated by calling the ```predict``` method on the model.

### ```test```
```python
def predict(self, testing_data):
        self.test_input, self.test_output = self.split_sequence(testing_data)
        self.num_test_samples = len(self.test_input)
        self.test_input = self.test_input.reshape((self.num_test_samples, self.window_size, self.num_features))
        self.test_predict = self.model.predict(self.test_input)
```
The ```predict``` method is called by the user with either test or the validation data as inputs. The
sequence specifed as input is first split into input, output pairs. Next the input is reshaped to the shape
required by Keras and the prediction is made by calling the ```predict``` method of the Sequential model.
All intermediate values are stored in instance variables so that they can be visualized later.

### ```visual_training```, ```visualize_testing```, ```visualize_validation```
All three visualize method are very similar. They plot the predicted output versus the actual output.
For the predicted output, the number of steps ahead needs to be specifed by the user. If ```look_ahead=5```,
for example, the user might want to visualize only the 1-step and 5-step ahead performance. In this case
```lookaheads=[1,5]``` needs to be specified. Each predicted plot is plotted in a different color. The 
details of ```visualize_testing``` are presented below, as a representative sample of these three methods.
```python
    def visualize_testing(self, lookaheads):
        plt.figure(1, figsize=(11, 8.5))
        plt.plot(self.test_output[:, 0], label='test', color='black', linewidth=2, zorder=20)
        for k in lookaheads:
            skip_values = np.array((k - 1) * (np.nan,))
            test_predict = np.append(skip_values, self.test_predict[:, k - 1])
            plt.plot(test_predict,
                     label=f'test predict {k}-step ahead',
                     color=self.colors[k-1 % 6], linewidth=1, zorder=10)
        plt.title('Performance on Testing Data')
        plt.legend(loc='best')
        plt.show()
```

# A Case Study in Financial Timeseries Prediction

## Introduction
Financial data is one of the most well known and commonly used example of time series data. Stock prices
and trading volumes have been meticulously recorded for a multitude of stocks for many years. There are many
public and freely available sources for accessing financial stock market data. We will be using 
[Alpha Vantage](https://www.alphavantage.co/) for this study.

[Alpha Vantage](https://www.alphavantage.co/) provides free APIs that can be used for downloading data in
JSON and CSV formats. Navigating to the API document page tells us that all the APIs provide realtime data
and the APIs are categorized as:

1. Stock Time Series Data
1. Physical and Digital/Crypto Currencies
1. Technical Indicators
1. Sector Performances

We will be using the Stock Time Series Data in our study. An example API call, to download data in the csv
format is
```
https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=MSFT&apikey=demo&datatype=csv
```
In this example, we are downloading the daily Microsoft stock values (```symbol=MSFT```) as a time series (```function=TIME_SERIES_DAILY```). These values are not adjusted. ```datatype=csv``` indicates that we are
trying to download the values as a CSV file. We also notice ```apikey=demo``` in the above query. This is 
the demo API key let's us download samples of data. But to download the full datasets, we are required to sign
up for a proper API key.

While we could use the above API to download the datasets, and load the CSV file into a ```pandas DataFrame```,
it would be nicer if there was a more pythonic way to automate this process and be able to make the API
calls from within our Python code. Such a solution is provided by the 
[alpha_vantage](https://alpha-vantage.readthedocs.io/en/latest/) Python package. This package can be 
installed via ```pip```. After ```pip install```ing the package, we can do the following in our Python code:
```python
from alpha_vantage.timeseries import TimeSeries
import os

ts = TimeSeries(key=os.environ.get('ALPHAVANTAGE_API_KEY'), output_format='pandas')
ts_data, ts_metadata = ts.get_daily_adjusted(symbol='AAPL', outputsize='full')
```
Here we are downloading the full dataset for Apple stock (```AAPL```) using our Alpha Vantage API key. The
key is stored privately as an environment variable whose value is accessed via
```python
os.environ.get('ALPHAVANTAGE_API_KEY')
```
Since we are specifying ```output_format='pandas'```, ```ts_data``` is created as a regular ```DataFrame``` 
object. Also, since we are calling the ```get_daily_adjusted``` method on ```ts```, we are obtaining the
daily adjusted values.

[alpha_vantage](https://alpha-vantage.readthedocs.io/en/latest/) simplifies our workflow tremendously and
we will be using that exclusively in our analysis.

## Predicting the Adjusted Closing Price of Stock Data
For our first study, we will be training an LSTM to predict the daily adjusted closing price for Apple stock. 
### Obtaining the Dataset
The first step is to download the dataset, which we do as follows
```python
ts = TimeSeries(key=os.environ.get('ALPHAVANTAGE_API_KEY'), output_format='pandas')
ts_data, ts_metadata = ts.get_daily_adjusted(symbol='AAPL', outputsize='full')
```
### Preprocessing the Dataset
This step is not required, since ```alpha_vantage``` automatically saves the downloaded data as a ```DataFrame``` 
object.

### Exploratory Analysis of the Timeseries Data
Let's first inspect the dataset to get an estimate of the size of the dataset and to check if any values are
missing. 
```python
print(ts_data.info())
```
prints
```
<class 'pandas.core.frame.DataFrame'>
Index: 5315 entries, 1998-01-02 to 2019-02-15
Data columns (total 8 columns):
1. open                 5315 non-null float64
2. high                 5315 non-null float64
3. low                  5315 non-null float64
4. close                5315 non-null float64
5. adjusted close       5315 non-null float64
6. volume               5315 non-null float64
7. dividend amount      5315 non-null float64
8. split coefficient    5315 non-null float64
dtypes: float64(8)
```
We see that we have eight columns, **other than** the index, and all the values are present. The data points
range from January 1998 to February 2019, about 21 years of data. The data values look like
```
            1. open  2. high  ...  7. dividend amount  8. split coefficient
date                          ...                                          
1998-01-02    13.63    16.25  ...                 0.0                   1.0
1998-01-05    16.50    16.56  ...                 0.0                   1.0
1998-01-06    15.94    20.00  ...                 0.0                   1.0
1998-01-07    18.81    19.00  ...                 0.0                   1.0
1998-01-08    17.44    18.62  ...                 0.0                   1.0
           ...                                          
2019-02-11   171.05  171.2100  ...                 0.0                   1.0
2019-02-12   170.10  171.0000  ...                 0.0                   1.0
2019-02-13   171.39  172.4800  ...                 0.0                   1.0
2019-02-14   169.71  171.2615  ...                 0.0                   1.0
2019-02-15   171.25  171.7000  ...                 0.0                   1.0
```
We see that the date column has already been set as the index column for us, saving us a step in the preprocessing.
We are interested in the adjusted close values. This is contained in the column with index 4. This can be visualized 
by
```python
ts_data.iloc[:, 4].plot()
plt.title('Adjusted Closing Price of AAPL Stock over the past 21 years')
plt.show()
```
and is shown in Figure \ref{aapladjc}

![AAPL Adjusted Closing Price.\label{aapladjc}](figures/aapl-adjusted-closing-price.png){width=60%}

### Splitting the data into Training, Testing, and Validation Sets
As mentioned previously, we need to maintain the sequential ordering of the data values while splitting the
data set into training, testing, and validation sets. To this end, we assign the first 3500 data points for
training, the next 1200 data points for testing, and the final 615 data points for validation
```python
ts_ad_close = ts_data.iloc[:, 4]

training = ts_ad_close[:3500]
testing = ts_ad_close[3500:4700]
validation = ts_ad_close[4700:]

print(training.shape)
print(testing.shape)
print(validation.shape)
```
```
(3500,)
(1200,)
(615,)
```

### Making Predictions
The next step is to train the predictor and evaluate it's performance. We will initially use only the training
and testing data sets; once we finalize on our predictor parameters, we do a final evaluation using the validation
set.

Let's start by using the following parameters:

1. We use the past three data points to predict the next two points (i.e. window_size=3 and look_ahead=2)
1. We use two hidden layers, each with 50 nodes.
1. We train for 100 epochs

The above set of parameter values can be concisely represented by the following tuple
```python
(3, 2, [50,50], 100)
```
We will use this notation in the discussion below.

The results of this run are shown in the following figures. Figure \ref{aapl-run01-train} shows the 
performance of the LSTM predictor on the training data

![AAPL Training Data Performance (3,2,[50,50],100).\label{aapl-run01-train}](figures/aapl-3-2-50x50-100-train_pred.png){width=50%}

Not surprisingly the prediction performance is good, though not much should be read into it. What about 
the performance on the test data? Figure \ref{aapl-run01-test} shows this performance

![AAPL Testing Data Performance (3,2,[50,50],100).\label{aapl-run01-test}](figures/aapl-3-2-50x50-100-test_pred.png){width=50%}

We see that the performance on the test data is also reasonably good with the 1-step prediction, not surprisingly, being better than the 2-step ahead prediction. However, we notice both underestimate the 
peaks and overestimate the valleys. 

Do we actually need to train for 100 epochs? Figure \ref{aapl-run01-loss-vs-epochs} tells us probably not.

![Loss vs Epochs (3,2,[50,50],100).\label{aapl-run01-loss-vs-epochs}](figures/aapl-3-2-50x50-100-loss-vs-epochs.png){width=50%}

### Exploring the Parameter Space
In this section, we will explore the following parameter settings:
1. ```(3,5, [50x50], 50)```
1. ```(3,5, [50], 50)```

The performace of the network, when it is asked to predict farther out, with 50 epochs of training, is as
follows. Figure \ref{aapl-run02-train} shows the performance on the training data, and Figure \ref{aapl-run02-test} shows the performance on the testing data. In both plots, we show only the 1-step and 5-step ahead predictions, for maximum contrast. 

![AAPL Training Data Performance (3,5,[50,50],50).\label{aapl-run02-train}](figures/aapl-3-5-50x50-50-train_pred.png){width=50%}

![AAPL Testing Data Performance (3,5,[50,50],50).\label{aapl-run02-test}](figures/aapl-3-5-50x50-50-test_pred.png){width=50%}

Surprisingly, the 5-step ahead prediction seems to be doing (slightly?) better than the 1-step ahead 
prediction! This could be due to overfitting. Let's check this by running the predictior with a single hidden
layer.

Figure \ref{aapl-run03-train} shows the performance of the training data, and Figure \ref{aapl-run03-test}
shows the performance on the testing data. These runs are for a predictor with a single hidden layer 
consisting of 50 nodes, using 3 past points to predicts 5 points into the future.

![AAPL Training Data Performance (3,5,[50],50).\label{aapl-run03-train}](figures/aapl-3-5-50-50-train_pred.png){width=50%}

![AAPL Testing Data Performance (3,5,[50],50).\label{aapl-run03-test}](figures/aapl-3-5-50-50-test_pred.png){width=50%}

We notice that our suspicion of over-fitting might be well founded. This network performs just as well, if
not slightly better, than the network with two hidden layers. The 1-step ahead prediction is closer to the
actual value than the 5-step ahead prediction, which is more intuitive. The performance of the 5-step ahead
prediction is not too shabby, either. Both the 1-step and 5-step ahead predictions continue to underestimate
the peaks and overestimate the valleys.

### Evaluation on the Validation Data
Based on our runs from the previous section, we will pick the simplest network configuration that gives
the desired performance and use that on our validation data. Thus, we use the 
```python
(3,5,[50], 50)
```
configuration. Figure \ref{aapl-validation} shows the performance of the network on the validation data, 
using the 1-step ahead and 5-step ahead predictions

![AAPL Validation Data Performance (3,5,[50],50).\label{aapl-validation}](figures/aapl-3-5-50-50-valid_pred.png){width=50%}

We notice that the performance is not as good as that on the testing data. One reason for this could be that
there is a gap between the training data and the validation data; testing data is contiguous with training
data, while the validation data is not.

# Summary
This report covered Timeseries predictions using Recurrent Neural Networks. We discussed what timeseries
are, and why we need a special class of networks for making time series predictions. We then covered what
Recurrent Neural Networks are, with a special emphasis on a particular type of RNNs known as Long Short-Term
Memory networks. We then discussed how a specific implementation of LSTMs in Keras, followed by the discussion
of the implementation of a wrapper class that uses the Keras API.

Next we discussed applying the software to make actual timeseries prediction on stock price data. We talked
about to download the data that would be used for prediction. Next we experimented with various network settings
to see how it affects prediction performance. We ended up with a network that was easy to setup, and had a reasonably
good performance.

The subject are addressed in this report is vast, and we covere a miniscule subset of this. We discussed only
univariate timeseris; multivariate timeseries are also very common and important. LSTMs are not the only kind 
of RNNs, and Keras is not the only library that can be used. We did not discuss the backend used by Keras 
(it was TensorFlow). Finally, timeseries prediction is a classical field that has been around for about
a hundred years.

This report does highlight an interesting observation -- *using* deep learning techniques is relatively easy,
but understanding what the neural network is actually doing is much more difficult. Contrast this with the
classical timeseries analysis, where the amount of material that needs to be mastered is vast, but the predictions
themselves are on rigorous mathematical footing.
