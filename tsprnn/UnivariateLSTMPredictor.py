import numpy as np
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, LSTM
import pandas as pd


class UnivariateLSTMPredictor:
    def __init__(self, window_size, look_ahead, hidden_layer_dimensions, num_epochs):
        self.window_size = window_size
        self.look_ahead = look_ahead
        self.num_hidden_layers = len(hidden_layer_dimensions)
        self.hidden_layer_dimensions = hidden_layer_dimensions
        self.num_epochs = num_epochs
        self.train_input = None
        self.train_output = None
        self.train_predict = None
        self.test_input = None
        self.test_output = None
        self.test_predict = None
        self.history = None
        self.model = None
        self.num_features = 1
        self.num_test_samples = 0
        self.colors = ['r', 'g', 'b', 'c', 'y', 'm']
        self.initialize_sequential_model()

    def initialize_sequential_model(self):
        self.model = Sequential()
        self.model.add(
            LSTM(self.hidden_layer_dimensions[0],
                 activation='relu',
                 input_shape=(self.window_size, self.num_features),
                 return_sequences=True if self.num_hidden_layers > 1 else False))
        for ndx in range(1, self.num_hidden_layers):
            self.model.add(
                LSTM(self.hidden_layer_dimensions[ndx],
                     activation='relu',
                     return_sequences=True if ndx < self.num_hidden_layers-1 else False))
        self.model.add(Dense(self.look_ahead))
        self.model.compile(optimizer='adam', loss='mse')

    def split_sequence(self, raw_sequence):
        inputs = []
        outputs = []
        for i in range(len(raw_sequence)):
            input_start_index = i
            input_stop_index = input_start_index + self.window_size
            output_start_index = input_stop_index
            output_stop_index = output_start_index + self.look_ahead
            if output_stop_index <= len(raw_sequence):
                inputs.append(raw_sequence[input_start_index:input_stop_index])
                outputs.append(raw_sequence[output_start_index:output_stop_index])
        return np.array(inputs), np.array(outputs)

    def train(self, training_data):
        self.train_input, self.train_output = self.split_sequence(training_data)
        input_shape = self.train_input.shape
        self.train_input = self.train_input.reshape((input_shape[0], input_shape[1], self.num_features))
        self.history = self.model.fit(self.train_input, self.train_output, epochs=self.num_epochs, verbose=0)
        self.train_predict = self.model.predict(self.train_input)

    def predict(self, testing_data):
        self.test_input, self.test_output = self.split_sequence(testing_data)
        self.num_test_samples = len(self.test_input)
        self.test_input = self.test_input.reshape((self.num_test_samples, self.window_size, self.num_features))
        self.test_predict = self.model.predict(self.test_input)

    def visualize_training(self, lookaheads):

        plt.figure(1, figsize=(11, 8.5))

        plt.plot(self.train_output[:, 0], label='train', color='black', linewidth=2, zorder=20)

        for k in lookaheads:
            skip_values = np.array((k-1)*(np.nan,))
            train_predict = np.append(skip_values, self.train_predict[:, k-1])
            plt.plot(train_predict,
                     label=f'train predict {k}-step ahead',
                     color=self.colors[k-1 % 6], linewidth=1, zorder=10)
        plt.legend(loc='best')
        plt.title('Performance on Training Data')
        plt.show()

    def visualize_testing(self, lookaheads):

        plt.figure(1, figsize=(11, 8.5))

        plt.plot(self.test_output[:, 0], label='test', color='black', linewidth=2, zorder=20)

        for k in lookaheads:
            skip_values = np.array((k - 1) * (np.nan,))
            test_predict = np.append(skip_values, self.test_predict[:, k - 1])
            plt.plot(test_predict,
                     label=f'test predict {k}-step ahead',
                     color=self.colors[k-1 % 6], linewidth=1, zorder=10)
        plt.title('Performance on Testing Data')
        plt.legend(loc='best')
        plt.show()

    def visualize_validation(self, lookaheads):

        plt.figure(1, figsize=(11, 8.5))

        plt.plot(self.test_output[:, 0], label='validation', color='black', linewidth=2, zorder=20)

        for k in lookaheads:
            skip_values = np.array((k - 1) * (np.nan,))
            test_predict = np.append(skip_values, self.test_predict[:, k - 1])
            plt.plot(test_predict,
                     label=f'validation predict {k}-step ahead',
                     color=self.colors[k-1 % 6], linewidth=1, zorder=10)
        plt.title('Performance on Validation Data')
        plt.legend(loc='best')
        plt.show()

    def plot_loss_vs_epochs(self):
        plt.figure(1, figsize=(11, 8.5))
        plt.plot(self.history.history['loss'])
        plt.title('Loss vs epoch number')
        plt.xlabel('Epoch Number')
        plt.ylabel('RMS Loss')
        plt.show()


if __name__ == '__main__':
    # load and preprocess
    epd = pd.read_csv('/home/rkk/Dropbox/timeseries-datasets/gmails-per-day.csv')
    epd['date'] = epd.date.astype('str')
    epd['date'] = pd.to_datetime(epd['date'], format='%y%m%d')
    epd = epd.set_index(['date'])
    epd_data = epd.number.values

    # split into training, testing and validation sets
    training_set = epd_data[:200]
    testing_set = epd_data[200:]

    uniLSTMPredictor = UnivariateLSTMPredictor(
        window_size=3,
        look_ahead=2,
        hidden_layer_dimensions=[50, 50, 50],
        num_epochs=50)
    uniLSTMPredictor.train(training_set)
    uniLSTMPredictor.predict(testing_set)
    uniLSTMPredictor.visualize_training([1, 2])
    uniLSTMPredictor.plot_loss_vs_epochs()
    uniLSTMPredictor.visualize_testing([1, 2])
