# README

The directory structure for the project is as follows:
.
├── case-study.ipynb
├── case-study.py
├── chapter-01.py
├── data
│   └── gmails-per-day.csv
├── eisvogel-book.latex
├── figures
├── mdtopdf
├── pip-requirements.txt
├── README.md
├── tsprnn
│   ├── __init__.py
│   └── UnivariateLSTMPredictor.py
├── TSPRNN.md
└── TSPRNN.pdf

1. Install all required packages using pip install -r pip-requirements.txt --upgrade in the root
    TSPRNN folder, **after** setting up a python3 virtualenv.
2. Install the alphavantage api key in ALPHAVANTAGE_API_KEY environment variable.
3. Type 'python case-study.py' in the TSPRNN folder to run the case study code from the command line.
4. Type 'jupyter notebook' in the root folder and open case-study.ipynb to do the same from Jupyter.
5. TSPRNN.md contains the source for the report. TSPRNN.pdf was generated from this report
using the mdtopd script. mdtopdf uses [pandoc](https://pandoc.org/) to genrate the output. Install
the included eisvogel-book.latex template in ~/.pandoc/templates/ for the report generation to work.
6. All figures used in the report are found in the figures directory.
7. To run the mini case-study code from chapter 1, navigate to the root folder and type python chapter-01
